#+title: Definições em early-init.el

* Remove o aviso sobre o pacote cl obsoleto

#+begin_src emacs-lisp :results value
  
  (setq byte-compile-warnings '(cl-functions))

#+end_src

* Configurações da interface

#+begin_src emacs-lisp :results value

  (setq inhibit-startup-message t)     ; Adeus, buffer assustador!
  (tool-bar-mode -1)                   ; Oculta a barra de ferramentas
  (menu-bar-mode -1)                   ; Oculta a barra de menu
  (scroll-bar-mode -1)                 ; Oculta a barra de rolagem
  (tooltip-mode -1)                    ; Oculta dicas
  
#+end_src

* Exibe numeração de linhas

#+begin_src emacs-lisp :results value

  (global-display-line-numbers-mode t)
  (setq-default display-line-numbers-type 'relative)

#+end_src

* Exibe coluna atual na modeline

#+begin_src emacs-lisp :results value

  (column-number-mode t)

#+end_src

* Exibe destaque de linha

#+begin_src emacs-lisp :results value

  (global-hl-line-mode t)

#+end_src

* Rolagem mais suave

#+begin_src emacs-lisp :results value

  (setq mouse-wheel-scroll-amount '(2 ((shift) . 1)) ; 2 linhas por vez
        mouse-wheel-progressive-speed nil            ; Não acelera a rolagem
        mouse-wheel-follow-mouse 't                  ; Rola a janela sob o mouse
        scroll-step 1)                               ; Rola 1 linha com teclado (nos limites)

#+end_src

* Alertas visuais

#+begin_src emacs-lisp :results value

  (setq visible-bell t)

#+end_src

* Espaçamento das bordas laterais

#+begin_src emacs-lisp :results value

  (set-fringe-mode 10)

#+end_src

* Ajustes para a minha sanidade mental

#+begin_src emacs-lisp :results value

  (global-unset-key (kbd "C-z")) ; Desabilita Ctrl-Z (suspend frame)
  (delete-selection-mode t)      ; O texto digitado substitui a seleção

#+end_src

* Dá foco às janelas recém-criadas

#+begin_src emacs-lisp :results value

  (global-set-key "\C-x2" (lambda ()
                            (interactive)
                            (split-window-vertically) (other-window 1)))
  (global-set-key "\C-x3" (lambda ()
                            (interactive)
                            (split-window-horizontally) (other-window 1)))

#+end_src

* Quebras de linha

#+begin_src emacs-lisp :results value

  (global-visual-line-mode t)

#+end_src

* Tipo de cursor (box, bar ou hbar)

#+begin_src emacs-lisp :results value

  (setq-default cursor-type 'bar)

#+end_src

* Habilita alterar caixa de seleções

#+begin_src emacs-lisp :results value

  (put 'upcase-region 'disabled nil)
  (put 'downcase-region 'disabled nil)

#+end_src

* Define o modo de novos buffers

#+begin_src emacs-lisp :results value

  (setq initial-major-mode 'fundamental-mode)
  (setq initial-buffer-choice 'debmx/new-buffer)

#+end_src

* Movendo os backups

#+begin_src emacs-lisp :results value

  (setq backup-directory-alist `(("." . ,(expand-file-name "saves/" user-emacs-directory))))

#+end_src

* Manda as custom settings para outro arquivo

#+begin_src emacs-lisp :results value

  (setq custom-file "~/.emacs.d/custom.el")
  (load custom-file)

#+end_src

* Carrega minhas funções

#+begin_src emacs-lisp :results value

  (load "~/.emacs.d/debmx/debmx.el")

#+end_src

* Define o modo de novos buffers

#+begin_src emacs-lisp :results value

  (setq initial-major-mode 'fundamental-mode)
  (setq initial-buffer-choice 'debmx/new-buffer)

#+end_src

* Diretório adicional de arquivos Info

#+begin_src emacs-lisp :results value

  (add-to-list 'Info-default-directory-list "/home/blau/info")
  (add-hook 'Info-mode-hook
    (lambda ()
      (setq Info-additional-directory-list Info-default-directory-list)))

#+end_src
