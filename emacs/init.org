#+title: Definições do init.el

* Definição de fontes

** Fonte padrão

#+begin_src emacs-lisp :results value

  (set-face-attribute 'default nil :font "Roboto Mono" :height 120)

#+end_src

** Fonte de largura fixa

#+begin_src emacs-lisp :results value

  (set-face-attribute 'fixed-pitch nil :font "Roboto Mono" :height 120)

#+end_src

** Fonte de largura varável

#+begin_src emacs-lisp :results value

  (set-face-attribute 'variable-pitch nil :font "Open Sans" :height 120 :weight 'regular)

#+end_src

* Define o modo de alguns arquivos e scripts

#+begin_src emacs-lisp :results value

  (setq auto-mode-alist (append '(("\\.bash_aliases\\'" . sh-mode)) auto-mode-alist))

#+end_src

* Inicialização do sistema de pacotes

#+begin_src emacs-lisp :results value

  (require 'package)
  (setq package-archives '(("melpa" . "https://melpa.org/packages/")
                           ("org"   . "https://orgmode.org/elpa/")
                           ("elpa"  . "https://elpa.gnu.org/packages/")))
  (package-initialize)
  (unless package-archive-contents
    (package-refresh-contents))

#+end_src

* Incialização do 'use-package'

#+begin_src emacs-lisp :results value

  (unless (package-installed-p 'use-package)
    (package-install 'use-package))
  (require 'use-package)
  (setq use-package-always-ensure t)
  
#+end_src

* Atualizações automáticas

#+begin_src emacs-lisp :results value

  (use-package auto-package-update
    :custom
    (auto-package-update-interval 7)
    (auto-package-update-prompt-before-update t)
    (auto-package-update-hide-results t)
    :config
    (auto-package-update-maybe)
    (auto-package-update-at-time "21:00"))

#+end_src

* All-the-icons

Para ter os ícones, depois de avaliado o ~use-package~, é preciso executar:

~M-x all-the-icons-install-fonts~

#+begin_src emacs-lisp :results value

  (use-package all-the-icons
    :if (display-graphic-p))

#+end_src

* Temas (=doom-themes=)

#+begin_src emacs-lisp :results value

  (use-package doom-themes
    :ensure t
    :config
    (setq doom-themes-enable-bold t
          doom-themes-enable-italic t)
    (load-theme 'doom-palenight t)
    (doom-themes-visual-bell-config)
    (doom-themes-org-config))
  

#+end_src

Outras opções em ~:config~

** Tema para =neotree=

#+begin_src emacs-lisp :results value

  (doom-themes-neotree-config)
  
#+end_src

** Tema para =treemacs=

#+begin_src emacs-lisp :results value

  (setq doom-themes-treemacs-theme "doom-palenight")
  (doom-themes-treemacs-config)
  
#+end_src

* Tema da mode line (=doom-modeline=)

#+begin_src emacs-lisp :results value

  (use-package doom-modeline
    :ensure t
    :hook
    (after-init . doom-modeline-mode)
    :custom
    (doom-modeline-height 30)
    :config
    (setq doom-modeline-enable-word-count t))

#+end_src

** Força a minha definição de fontes na mode line

#+begin_src emacs-lisp :results value

  (set-face-attribute 'mode-line nil :font "Roboto Mono" :height 80)
  (set-face-attribute 'mode-line-inactive nil :font "Roboto Mono" :height 80)

#+end_src

* Cores diferentes para delimitadores (=rainbow-delimiters=)

#+begin_src emacs-lisp :results value

  (use-package rainbow-delimiters
    :hook (prog-mode . rainbow-delimiters-mode))

#+end_src

* Fechamento de delimitadores (=smartparens=)

#+begin_src emacs-lisp :results value

  (use-package smartparens
    :ensure t
    :init
    (require 'smartparens-config)
    (smartparens-global-mode t)
    :config
    (show-smartparens-mode t))

#+end_src

* Remove/abrevia modos menores na mode line (=diminish=)

#+begin_src emacs-lisp :results value

  (use-package diminish)

#+end_src

* Informações de complementos dos atalhos (=which-key=)

#+begin_src emacs-lisp :results value

  (use-package which-key
    :init (which-key-mode)
    :diminish which-key-mode
    :config
    (setq which-key-idle-delay 0.5)
    (setq which-key-show-early-on-C-h t))

#+end_src

* Autocomplemento (=company=)

#+begin_src emacs-lisp :results value

  (use-package company
    :hook (after-init . global-company-mode))

#+end_src

* Interface para complemento de comandos (=ivy=)

#+begin_src emacs-lisp :results value

  (use-package ivy
    :diminish
    :bind (("C-s" . swiper)
           :map ivy-minibuffer-map
           ("TAB" . ivy-alt-done)
           ("C-l" . ivy-alt-done)
           ("C-j" . ivy-next-line)
           ("C-k" . ivy-previous-line)
           :map ivy-switch-buffer-map
           ("C-k" . ivy-previous-line)
           ("C-l" . ivy-done)
           ("C-d" . ivy-switch-buffer-kill)
           :map ivy-reverse-i-search-map
           ("C-k" . ivy-previous-line)
           ("C-d" . ivy-reverse-i-search-kill))
    :config
    (ivy-set-actions
     ;; Options for selected with M-o
     'counsel-find-file
     '(("w" find-file-other-window "other window")
       ("f" find-file-other-frame "other frame")
       ("c" copy-file "copy")
       ("b" counsel-find-file-cd-bookmark-action "cd dir")
       ("x" counsel-find-file-extern "other application")
       ("r" counsel-find-file-as-root "open as root")))
    (ivy-mode 1))

#+end_src

* Mais detalhes para =ivy= e =counsel=

#+begin_src emacs-lisp :results value

  (use-package ivy-rich
    :init (ivy-rich-mode 1))

#+end_src

* Framework de complementos para o =ivy=

#+begin_src emacs-lisp :results value

  (use-package counsel
    :bind (("M-x"     . counsel-M-x)
           ("C-x b"   . counsel-ibuffer)
           ("C-x C-f" . counsel-find-file)
           :map minibuffer-local-map
           ("C-r" . 'counsel-minibuffer-history))
    :config
    (setq ivy-initial-inputs-alist nil)
    :custom
    (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only))

#+end_src

* Mais opções de busca (=ivy-prescient=)

#+begin_src emacs-lisp :results value

  (use-package ivy-prescient
    :after counsel
    :config
    (ivy-prescient-mode 1)
    (prescient-persist-mode 1)
    (setq ivy-prescient-retain-classic-highlighting t
          prescient-sort-length-enable nil))

#+end_src

* Mais opções de complemento =company-prescient=

#+begin_src emacs-lisp :results value

  (use-package company-prescient
    :after company
    :config
    (company-prescient-mode 1))

#+end_src

* Mover linhas e regiões (=drag-stuff=)

#+begin_src emacs-lisp :results value

  (use-package drag-stuff)
  (drag-stuff-global-mode 1)

#+end_src

* Definição de atalhos (=general=)

#+begin_src emacs-lisp :results value

  (use-package general)

#+end_src

** Atalhos personalizados

#+begin_src emacs-lisp :results value

  (general-define-key
    "<escape>"     'keyboard-escape-quit
    "C-z C-<down>" '(counsel-switch-buffer       :which-key "switch-buffer")
    "M-<next>"     '(drag-stuff-down             :which-key "move text down")
    "M-<prior>"    '(drag-stuff-up               :which-key "move text up")
    "C-z b"        '(bookmark-jump               :which-key "bookmark jump")
    "C-z w"        '(bookmark-jump-other-window  :which-key "bookmark other window")
    "C-z s"        '(bookmark-set                :which-key "bookmark set")
    "C-z l"        '(bookmark-bmenu-list         :which-key "bookmark list")
    "C-z f"        '(ffap                        :which-key "file at point")
    "C-z r"        '(recentf-open-files          :which-key "recent files menu")
    "C-z p"        '(package-refresh-contents    :which-key "refresh package list")
    "C-z C-b"      '(eval-buffer                 :which-key "eval expression")
    "C-z C-e"      '(eval-expression             :which-key "eval expression")
    "C-z C-r"      '(eval-region                 :which-key "eval region")
    "C-z C-t"      '(vterm                       :which-key "launch vterm")
    "C-z <left>"   '(windmove-left               :which-key "focus window left")
    "C-z <right>"  '(windmove-right              :which-key "focus window right")
    "C-z <up>"     '(windmove-up                 :which-key "focus window up")
    "C-z <down>"   '(windmove-down               :which-key "focus window down")
    "C-z ,"        '(beginning-of-buffer         :which-key "beggining of buffer")
    "C-z ."        '(end-of-buffer               :which-key "end of buffer")
    "C-z i"        '(debmx/open-init-file        :which-key "user init file")
    "C-z C-d"      '(debmx/duplicate-line        :which-key "duplicate line")
    "C-z C-f"      '(debmx/new-buffer            :which-key "create new untitled buffer")
    "C-S-k"        '(debmx/kill-from-beginning   :which-key "kill from the start"))

#+end_src

* Ferramentas para desenvolvimento em PHP

#+begin_src emacs-lisp :results value

  (use-package php-mode
    :mode ("\\.php\\'" . php-mode))

  (use-package web-mode
    :mode ("\\.phtml\\.tpl\\.html\\.twig\\.html?\\'" . web-mode))

#+end_src

* Markdown mode

#+begin_src emacs-lisp :results value

  (use-package markdown-mode
    :commands (markdown-mode gfm-mode)
    :mode (("README\\.md\\'" . gfm-mode)
           ("\\.md\\'" . markdown-mode)
           ("shell-pages\\/.*\.txt\\'" . markdown-mode)
           ("\\.markdown\\'" . markdown-mode))
    :init (setq markdown-command "pandoc"))

#+end_src

* Projectile

#+begin_src emacs-lisp :results value

  (use-package projectile
    :diminish projectile-mode
    :bind-keymap ("C-c C-p" . projectile-command-map)
    :config (projectile-mode)
    :init
    (when (file-directory-p "~/gits")
      (setq projectile-project-search-path '("~/gits")))
    (setq projectile-switch-project-action #'projectile-dired))

  (use-package counsel-projectile
    :config (counsel-projectile-mode))

#+end_src

* Linter (=flycheck=)

#+begin_src emacs-lisp :results value

  (use-package flycheck
    :ensure t
    :init (global-flycheck-mode))

#+end_src

* Gerenciamento de Git (=magit=)

#+begin_src emacs-lisp :results value

  (use-package magit)

#+end_src

* Gerenciamento de arquivos (=dired=)

#+begin_src emacs-lisp :results value

  (use-package dired-single)

  (use-package dired
    :ensure nil
    :commands (dired dired-jump)
    :custom ((dired-listing-switches "-lFaGh1v --group-directories-first")
             (dired-dwim-target t)
             (delete-by-moving-to-trash t))
    :bind (("C-x C-j" . dired-jump)
           :map dired-mode-map
           ("C-c h" . dired-hide-dotfiles-mode)
           ("^"     . dired-single-up-directory)
           ("RET"   . dired-single-buffer)))

  (use-package dired-open
    :config (setq dired-open-extensions '(("png"  . "sxiv")
                                          ("jpg"  . "sxiv")
                                          ("jpeg" . "sxiv")
                                          ("gif"  . "sxiv")
                                          ("mkv"  . "mpv")
                                          ("mp4"  . "mpv"))))

  (use-package all-the-icons-dired
    :hook (dired-mode . all-the-icons-dired-mode))

  (use-package dired-hide-dotfiles
    :hook (dired-mode . dired-hide-dotfiles-mode))

#+end_src

* Org mode

** Hook das opções padrão

#+begin_src emacs-lisp :results value

  (defun debmx/defaults-org ()
    "Defaults for org mode."
    (org-indent-mode)
    (auto-fill-mode 0)
    ;; (variable-pitch-mode 1)
    (display-line-numbers-mode -1))

#+end_src
  
** Org-mode e org-babel

#+begin_src emacs-lisp :results value

  (use-package org
    :ensure org-plus-contrib
    :hook (org-mode . debmx/defaults-org)
    :config
    (setq org-support-shift-select t
          org-hide-emphasis-markers t
          header-line-format " "
          org-babel-default-header-args '((:results . "output"))
          org-confirm-babel-evaluate nil)
    (org-babel-do-load-languages
     'org-babel-load-languages
     '((awk     . t)
       (clojure . t)
       (css     . t)
       (lua     . t)
       (sed     . t)
       (shell   . t)
       (php     . t)
       (python  . t)
       (C       . t))))

  (require 'org-tempo)

  (add-to-list 'org-structure-template-alist '("awk" . "src awk"))
  (add-to-list 'org-structure-template-alist '("clj" . "src clojure"))
  (add-to-list 'org-structure-template-alist '("css" . "src css"))
  (add-to-list 'org-structure-template-alist '("el"  . "src emacs-lisp :results value"))
  (add-to-list 'org-structure-template-alist '("lua" . "src lua"))
  (add-to-list 'org-structure-template-alist '("sed" . "src sed"))
  (add-to-list 'org-structure-template-alist '("php" . "src php"))
  (add-to-list 'org-structure-template-alist '("py"  . "src python :python python3"))
  (add-to-list 'org-structure-template-alist '("sh"  . "src shell"))
  (add-to-list 'org-structure-template-alist '("cfg" . "src unix-config"))

  (use-package org-bullets
    :after org
    :hook (org-mode . org-bullets-mode)
    :custom
    (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

  ;; Replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  ;; Make sure org-indent face is available
  (require 'org-indent)

#+end_src

** Escala de títulos, padding, etc (não estou usando!)

#+begin_src emacs-lisp :results value

  ;; Title scaling
  
  (with-eval-after-load 'org-faces
    (dolist (face '((org-level-1 . 1.6)
                    (org-level-2 . 1.4)
                    (org-level-3 . 1.2)
                    (org-level-4 . 1.1)
                    (org-level-5 . 1.0)
                    (org-level-6 . 1.0)
                    (org-level-7 . 1.0)
                    (org-level-8 . 1.0)))
      (set-face-attribute (car face) nil :font "Roboto Mono" :weight 'bold :height (cdr face))))

  ;; Org mode padding

  (defun debmx/org-visual-fill()
      (setq visual-fill-column-width 100
            visual-fill-column-center-text t)
      (visual-fill-column-mode 1))

  (use-package visual-fill-column
      :defer t
      :hook (org-mode . debmx/org-visual-fill))

#+end_src

* Org Roam

#+begin_src emacs-lisp :results value

  (use-package org-roam
    :ensure t
    :init
    (setq org-roam-v2-ack t)
    :custom
    (org-roam-directory "~/notes")
    (org-roam-completion-everywhere t)
    :bind (("C-c n l" . org-roam-buffer-toggle)
           ("C-c n f" . org-roam-node-find)
           ("C-c n i" . org-roam-node-insert)
           :map org-roam-grep-map
           ("C-M-i"   . completion-at-point))
    :config
    (org-roam-db-autosync-enable))

#+end_src

* Cliente IRC (=ERC=)

#+begin_src emacs-lisp :results value

  (setq erc-server-coding-system '(utf-8 . utf-8)
      erc-server "irc.libera.chat"
      erc-nick "blau_araujo"
      erc-user-full-name "Blau Araujo"
      erc-track-shorten-start 8
      erc-autojoin-channels-alist '(("libera.chat" "#debxp"))
      erc-kill-buffer-on-part t
      erc-auto-query 'bury)

#+end_src

* Terminais

** Carrega o term-mode com suporte a 256 cores

#+begin_src emacs-lisp :results value

  (use-package eterm-256color
    :hook (term-mode . eterm-256color-mode))

#+end_src

** Vterm

Requer: ~cmake libtool-bin libvterm-dev~ (Debian)

#+begin_src emacs-lisp :results value

  (use-package vterm
    :commands vterm
    :config
    (setq vterm-max-scrollback 10000
          term-prompt-regexp "^[^#$%>\n]*[#$%>] *"))

#+end_src



