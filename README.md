# Experimentos com Emacs e Emasc Lisp

## Emacs

* [Minhas funções](emacs/debmx.org)
* [Meu early-init.el](emacs/early-init.org)
* [Meu init.el](emacs/init.org)

## Emacs Lisp

* [Emacs Lisp #0 - Introdução](elisp/elisp-00.org)
