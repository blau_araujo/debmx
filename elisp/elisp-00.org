#+title: Emacs Lisp #0 - Introdução

* A quem se destina esta série

- Não-programadores curiosos sobre a linguagem utilizada para /hackear/ o Emacs.
- Programadores interessados em uma introdução rápida ao Emacs Lisp.
- A mim mesmo, já que eu só consigo aprender alguma coisa se eu conseguir explicar.

* O que é o Emacs em relação ao elisp

- Um *REPL* da linguagem elisp
- *REPL*: /Read, Eval, Print, Loop/
- Em português: /ciclos de leitura, avaliação e impressão/.
- Ou seja: o Emacs é um interpretador da linguagem Emacs Lisp.
  
* Elementos da linguagem

** Átomo: a unidade mais básica

#+begin_src emacs-lisp :results value

  15             ; avalia 15   (um inteiro)
  3.0            ; avalia 3.0  (um flutuante)
  path-separator ; avalia ":"  (uma variável interna contendo a string ":")
  "olá, mundo"   ; avalia "olá, mundo" (uma string)

#+end_src

** Expressões: listas de átomos

- Expressões são formas sintáticas que expressam valores.
- Todos os elementos sintáticos o elisp expressam valores, mesmo que o valor seja nulo (=nil=).
- Em elisp, =nil= tem o mesmo significado e valor de =false=.
- Qualquer valor diferente de =nil= é visto como verdadeiro (=true=)
- Quando a sintaxe for inválida, nós teremos um erro.

*** Listas
  
#+begin_src emacs-lisp :results value

  (1 2 3)         ; Erro: função inválida!
  (quote (1 2 3)) ; Avalia a lista (1 2 3)
  '(1 2 3)        ; Outra forma para (quote (lista))
  '()             ; Uma lista vazia avalia 'nil' (nulo)
  ()              ; Também avalia 'nil'
  nil             ; Ou apenas 'nil'

#+end_src

*** Chamadas de funções

#+begin_src emacs-lisp :results value

  (+ 10 25)                   ; soma 10 e 25 e avalia 35
  (defvar nome "Blau Araujo") ; atribui "Blau Araujo" a 'nome' e avalia 'nome'
  (message nome)              ; avalia 'nome' e exibe 'Blau Araujo'
  (null nil)                  ; avalia se nil é nil e retorna 't' (true)
  (null '())                  ; avalia 't'

#+end_src

*** Manipulando listas

- *CAR*: conteúdo na parte do endereço do registro
- *CDR*: conteúdo na parte do decremento do registro
- São apenas nomes históricos para o primeiro (=car=) e os demais elementos (=cdr=) de uma lista.

#+begin_src emacs-lisp :results value

  (car '(1 2 3))  ; Avalia o primeiro elemento da lista: 1
  (cdr '(1 2 3))  ; Avalia o restante dos elementos da lista: 2 3
  (car '(42))     ; Avalia 42
  (cdr '(42))     ; O CDR de uma lista com apenas um elemento avalia 'nil'

#+end_src

- A função ~cons~ é utilizada para /construir/ listas.

#+begin_src emacs-lisp :results value

  (cons 1 '(2 3))                ; Avalia (1 2 3)
  (cons 1 (cons 2 (cons 3 '()))) ; Avalia (1 2 3)

#+end_src

- A função ~append~ anexa listas umas às outras.

#+begin_src emacs-lisp :results value

  (append '(1 2) '(3 4)) ; Avalia (1 2 3 4)
  (append 1 '(2 3))      ; Erro: todos os argumentos devem ser listas!
  (append '(1) '(2 3))   ; Avalia (1 2 3)

#+end_src

** Variáveis

- São =símbolos= que identificam valores que podem mudar.
- Com a função ~set~, o símbolo precisa ser antecedido do apóstrofe (/quote/).

#+begin_src emacs-lisp :results value

  (set 'minha-lista '(1 2 3 4)) ; Atribui a lista (1 2 3 4) ao símbolo `minha-lista'
  minha-lista                   ; Avalia (1 2 3 4)

#+end_src

- A função ~setq~ (/set quoted/) aplica internamente o apóstrofe aos símbolos.
- Além disso, ~setq~ permite a definição de vários pares de nomes e valores.
- A expressão com setq avalia o último valor atribuído na lista de pares.

#+begin_src emacs-lisp :results value

  (setq minha-lista '(4 5 6)) ; Atribui o novo valor (4 6 5) à 'minha-lista'
  minha-lista                 ; Avalia (4 5 6)
  (setq nome "Blau"
        sobrenome "Araujo"
        cidade "Osasco")      ; Avalia o último valor atribuído (Osasco)
  nome                        ; Avalia "Blau"
  sobrenome                   ; Avalia "Araujo"
  cidade                      ; Avalia "Osasco"

#+end_src

** Escopo de variáveis

*** Escopo global

- Definições de variáveis feitas com ~set~, ~setq~ e ~setq-default~ são globais em relação à sessão ou ao buffer, dependendo dos atributos da variável.
- Se uma variável existente possuir o atributo =buffer-local=, por exemplo, alterações feitas com ~set~ e ~setq~ terão efeito apenas no buffer em que a nova definição é feita.
- Independente do atributo =buffer-local=, alterações feitas com ~setq-default~ terão efeito em toda a sessão.
- Quando a variável não é =buffer-local=, ~set~, ~setq~ e ~setq-default~ terão o mesmo efeito sobre o escopo.
- Variáveis =buffer-local= podem ser criadas com ~setq-local~ ou tornadas buffer-locais com ~make-variable-buffer-local~.
- As formas especiais ~defvar~ e ~defvar-local~, são equivalentes a ~setq~ e ~setq-local~, respectivamente, *no que diz respeito ao escopo*, mas oferecem o recurso adicional de permitir a definição de um texto para documentação da variável.

*** Escopo local

- Para definir variáveis restritas ao escopo de uma expressão (*locais*), nós utilizamos ~let~ ou ~let*~.
- Sintaxe para ambas as formas: =(let (LISTAS_DE_VARIÁVEIS) CORPO)=

#+begin_src emacs-lisp :results value

  ;; Variáveis são visíveis apenas no contexto do CORPO. 
  (let ((a "banana")
        (b "zebra")
        (c 42))
    (format "Fruta: %s, bicho: %s, sentido da vida: %d" a b c))
  ;; Avalia: "Fruta: banana, bicho: zebra, sentido da vida: 42"

#+end_src

- Com a função ~let~, as variáveis só estão disponíveis para uso no =CORPO=.

#+begin_src emacs-lisp :results value

  (let ((a 1)
        (b 2)
        (c (+ a b)))     ; Isso não funciona!
    (format "c = %d" c)) ; Erro!

#+end_src

- Quando é necessário utilizar as novas variáveis na =LISTA_DE_VARIÁVEIS=, nós temos o ~let*~.

#+begin_src emacs-lisp :results value

  (let* ((a 1)
         (b 2)
         (c (+ a b)))    ; Usando `a' e `b' para definir `c'
    (format "c = %d" c)) ; Avalia "c = 3"

#+end_src

** Definição de funções

- A definição de funções é feita com a função ~defun~.

#+begin_src emacs-lisp :results value

  ;; Definição...
  (defun salve-simpatia ()
    "Funções devem ser descritas para documentação!"
    (message "Salve, simpatia!")) ; Avalia o símbolo definido: salve-simpatia

  ;; Chamada...
  (salve-simpatia) ; Exibe "Salve, simpatia!" na área de mensagens (echo area)

#+end_src

*** Definindo parâmetros obrigatórios

#+begin_src emacs-lisp :results value

  (defun soma-10 (i)
    "Soma 10 ao valor do argumento"
    (+ i 10))

  (soma-10 20) ; Avalia 30
  (soma-10)    ; Erro: número incorreto de argumentos

#+end_src

*** TODO Funções com números variáveis de argumentos

*** TODO Definindo valores opcionais

*** TODO Funções interativas

*** Recursão

- É quando uma função chama  a si mesma.

#+begin_src emacs-lisp :results value

  (defun fatorial (i)
    "Retorna o fatorial de um inteiro"
    (if (< i 1)
        1
        (* i (fatorial (- i 1)))))

  (fatorial 3) ; 6
  (fatorial 4) ; 24
  (fatorial 5) ; 120

#+end_src

*** Funções anônimas

- Uma função anônima é a expressão literal de uma função
- São chamadas de ~lambda~.

#+begin_src emacs-lisp :results value

  (lambda (v) (* v v ))     ; Não retorna nada porque o argumento 'v' não foi passado.
  ((lambda (v) (* v v )) 2) ; Passando 2, retorna 4 (o quadrado)

  (fset 'quadrado (lambda (v) (* v v ))) ; Associa o símbolo 'quadrado' à função lambda.
  (quadrado 4)                           ; 16

#+end_src

*** Funções de alta ordem

- São funções que recebem funções como argumentos ou retornam uma função.

#+begin_src emacs-lisp :results value

  ;; Passa cada valor na lista como argumento da função
  ;; e retorna uma lista com os retornos.
  (mapcar 'quadrado '(2 3 4)) ; (4 9 16)
  (mapcar 'quadrado '())      ; nil

  ;; Remove valores da lista que resultem em retornos 'nil'.
  (remove-if-not 'oddp '(0 1 2 3 4 5 6)) ; (1 3 5)

#+end_src

** Estruturas condicionais

- Macro ~when~: =(when CONDIÇÃO CORPO)=
- Quando =CONDIÇÃO= resulta em um valor *não-nulo*, a expressão em =CORPO= é avaliada.
- É como um =if= sem =else=.

#+begin_src emacs-lisp :results value

  (when (= (+ 2 2) 4)
    (message "O universo ainda não mudou as regras!"))
  ;; Imprime: "O universo ainda não mudou as regras!"
  
#+end_src

- Forma especial ~if~: =(if CONDIÇÃO THEN ELSE)=
- Quando =CONDIÇÃO= resulta em um valor *não-nulo*, a expressão em =THEN= é avaliada.
- Caso contrário, avalia a expressão em =ELSE=.
- =THEN= deve ser apenas uma expressão.
- =ELSE= pode ser zero ou mais expressões.

#+begin_src emacs-lisp :results value

  (defun par-ou-impar (n)
    "Retorna par ou ímpar."
    (if (= 0 (% n 2))
      "par"
      "ímpar"))
  
  (par-ou-impar 5) ; "ímpar"
  (par-ou-impar 8) ; "par"

#+end_src

- Forma especial ~cond~: =(cond CLÁUSULAS)=
- =CLÁUSULAS= pode ser uma ou mais expressões =(CONDIÇÃO CORPO)=
- Avalia cada uma das =CLÁUSULAS= até que uma =CONDIÇÃO= resulte em um valor não-nulo e tenha seu =CORPO= avaliado.

#+begin_src emacs-lisp :results value

  (defun num-to-bicho (i)
    "Retorna um bicho para cada número."
    (cond ((= i 1) "gato")
          ((= i 2) "cachorro")
          ((= i 3) "rato")
          (t "zebra")))

  (num-to-bicho 1) ; "gato"
  (num-to-bicho 2) ; "cachorro"
  (num-to-bicho 3) ; "rato"
  (num-to-bicho 9) ; "zebra"

#+end_src




